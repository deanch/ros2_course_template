/*! \file
 *
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 15.03.2021
 *
 * \copyright Copyright 2021 Chalmers
 *
 * #### License
 * All rights reserved.
 *
 * Software License Agreement (BSD License 2.0)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TURTLE_CTRL_H
#define TURTLE_CTRL_H

/*! \file TurtleCtrl.h
 *   \brief Controls the turtle to move from the current state to the desired state
 *
 *   Provides the following functionalities:
 *     - Subscriber to a TurtleStateStamped topic (desired state)
 *     - Publisher of a TurtleStateStamped topic (for the turtle visualizer)
 *     - TF frames
 */

// Eigen Library
#include <Eigen/Dense>
#include <Eigen/StdVector>

// Custom ros messages
#include <turtle_msgs/msg/turtle_state_stamped.hpp>

// ros library
#include "rclcpp/rclcpp.hpp"

// tf publisher
#include <tf2_ros/transform_broadcaster.h>

namespace turtle_examples
{
class TurtleCtrl : public rclcpp::Node
{
public:
  using TurtleStateMsg = turtle_msgs::msg::TurtleStateStamped;  ///< Definition for TurtleStateStamped message
  using SubTurtleState = rclcpp::Subscription<TurtleStateMsg>::SharedPtr;  ///< Definition for TurtleState subscriber
  using PubTurtleState = rclcpp::Publisher<TurtleStateMsg>::SharedPtr;     ///< Definition for TurtleState publisher
  using TurtleStateShPt = TurtleStateMsg::SharedPtr;                       ///< Definition for TurtleState shared ptr

private:
  SubTurtleState sub_cmd_pose_;         ///< Subscriber commanded turtle pose
  PubTurtleState pub_current_pose_;     ///< Publisher current turtle pose
  rclcpp::TimerBase::SharedPtr timer_;  ///< Timer to run a parallel process

  std::shared_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_;  ///< Publisher for TF (to display commanded pose)

  std::string frame_id_;             ///< frame id for the commanded pose
  std::string subs_topic_name_;      ///< topic name for the subscriber (commanded pose)
  std::string pub_topic_name_;       ///< topic name for the turtle state publisher (for visualization)
  Eigen::Matrix3d K_;                ///< Control gain matrix
  Eigen::Vector3d turtle_current_;   ///< Current turtle position vector 3X1
  std::vector<double> turtle_init_;  ///< Initial position of the turtle
  int ctrl_period_;                  ///< Control period in ms

  std::mutex data_mutex_;           ///< Mutex to protect the reading/writing process
  TurtleStateShPt turtle_cmd_msg_;  ///< Shared object to share the turtle state between the control loop and the
                                    ///< subscriber
  bool first_message_;              ///< Flag to control when we have the turtle state
  bool init_ctrl_;                  ///< Flag to set the turtle initial position using the TurtleState

public:
  /**
   * @brief Default constructor
   *
   */
  TurtleCtrl(/* args */);

  /**
   * @brief Destroy the Turtle Ctrl object
   *
   */
  ~TurtleCtrl();

  /**
   * @brief Control initialization, sets the control and node parameters
   *
   */
  void init();

  /**
   * @brief Callback function to receive the TurtleState topic
   *
   * @param msg message with the current Turtle state (pose and velocity)
   */
  void topic_callback(const TurtleStateShPt msg);

  /**
   * @brief Callback function for the control loop. Internal thread for the controller.
   *
   */
  void timer_callback();
};

}  // namespace turtle_examples

#endif