/*! \file
 *
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 15.03.2021
 *
 * \copyright Copyright 2021 Chalmers
 *
 * #### License
 * All rights reserved.
 *
 * Software License Agreement (BSD License 2.0)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TURTLE_VIS_H
#define TURTLE_VIS_H

/*! \file TurtleVis.h
 *   \brief Allows to visualize the CAD model of the turtle
 *
 *   Provides the following functionalities:
 *     - Subscriber for TurtleStateStamped topic
 *     - Publisher for the visual markers (to see the CAD model in rviz)
 *     - TF frames
 */

// Eigen Library
#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>

// Custom ros messages
#include <turtle_msgs/msg/turtle_state_stamped.hpp>

// Standard ros messages
#include <visualization_msgs/msg/marker.hpp>
#include <visualization_msgs/msg/marker_array.hpp>

// ros library
#include "rclcpp/rclcpp.hpp"

namespace turtle_examples
{
class TurtleVis : public rclcpp::Node
{
private:
  visualization_msgs::msg::Marker turtle_marker_;                                       ///< Visualization marker
  rclcpp::Subscription<turtle_msgs::msg::TurtleStateStamped>::SharedPtr subscription_;  ///< topic subscription to get
                                                                                        ///< the Turtle state
  rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr v_marker_publisher_;  ///< marker publisher for
                                                                                           ///< rviz
  std::shared_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_;  ///< tf publisher need for the
                                                                   ///< markers

  std::string marker_topic_name_;  ///< topic name for the marker array
  std::string marker_namespace_;   ///< namespace for the marker array
  std::string frame_id_;           ///< reference frame for all the objects, usually "map"
  std::string subs_topic_name_;    ///< the name of the ATRStateList topic which we
                                   ///< will subscribe to

public:
  /**
   * @brief Standar constructor
   *
   */
  TurtleVis(/* args */);

  /**
   * @brief Destroy the Turtle Vis object
   *
   */
  ~TurtleVis();

  /**
   * @brief initialize the parameters for the turtle visualization
   *
   */
  void init();

private:
  /**
   * @brief callback function for the topic subscriber
   *
   * @param msg message with the current Turtle state (pose and velocity)
   */
  void topic_callback(const turtle_msgs::msg::TurtleStateStamped::SharedPtr msg);
};

}  // namespace turtle_examples

#endif