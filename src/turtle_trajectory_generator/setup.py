from setuptools import setup

import os

from glob import glob

package_name = "turtle_trajectory_generator"

setup(
    name=package_name,
    version="0.0.1",
    packages=[package_name],
    py_modules=[
        "turtle_trajectory_generator.trajectory_generator_module",
        "turtle_trajectory_generator.submodules.my_python_module",
    ],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (os.path.join("share", package_name, "launch"), glob("launch/*_launch.py")),
        (os.path.join("share", package_name, "configs"), glob("configs/*.yaml")),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="dean",
    maintainer_email="deane@chalmers.se",
    author="Emmanuel Dean, Graylin Trevor Jay, Austin Hendrix",
    keywords=["ROS"],
    classifiers=[
        "Intended Audience :: Developers",
        "License :: BSD",
        "Programming Language :: Python",
        "Topic :: Software Development",
    ],
    description="Turtle trajectory functions for ros2 crash course.",
    license="BSD",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "teleop_twist_keyboard = turtle_trajectory_generator.scripts.teleop_twist_keyboard:main",
            "twist2cmd = turtle_trajectory_generator.scripts.twist2cmd:main",
            "poly_trajectory = turtle_trajectory_generator.scripts.poly_trajectory:main",
            "my_class_node = turtle_trajectory_generator.scripts.my_class_node:main",
        ],
    },
)
