# \file
#
#  \author Emmanuel Dean
#
#  \version 0.1
#  \date 15.03.2021
#
#  \copyright Copyright 2021 Chalmers
#
#  #### License
# All rights reserved.

# Software License Agreement (BSD License 2.0)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:

#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# /


import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    # Get the launch directory
    bringup_dir = get_package_share_directory("turtle_ctrl")

    # Launch configuration variables specific to simulation
    rviz_config_file = LaunchConfiguration("rviz_config_file")
    use_rviz = LaunchConfiguration("use_rviz")

    # Define arguments
    declare_rviz_config_file_cmd = DeclareLaunchArgument(
        "rviz_config_file",
        default_value=os.path.join(bringup_dir, "rviz", "turtle.rviz"),
        description="Full path to the RVIZ config file to use",
    )
    declare_use_rviz_cmd = DeclareLaunchArgument("use_rviz", default_value="True", description="Whether to start RVIZ")

    # RQT
    rqt_graph_node = Node(package="rqt_graph", executable="rqt_graph", output="screen")

    # Rviz node
    rviz_cmd = Node(
        condition=IfCondition(use_rviz),
        package="rviz2",
        executable="rviz2",
        name="rviz2",
        arguments=["-d", rviz_config_file],
        output="screen",
    )

    # -------- NODES

    # Turtle Visualization
    turtle_vis_yaml = os.path.join(get_package_share_directory("turtle_ctrl"), "config", "turtle_vis.yaml")
    turtle_vis = Node(
        package="turtle_ctrl",
        name="turtle_visualizer",
        executable="turtle_vis",
        parameters=[turtle_vis_yaml],
        output="screen",
    )

    # Turtle Control
    turtle_ctrl_yaml = os.path.join(get_package_share_directory("turtle_ctrl"), "config", "turtle_ctrl.yaml")
    turtle_ctrl = Node(
        package="turtle_ctrl",
        name="turtle_control",
        executable="turtle_control",
        parameters=[turtle_ctrl_yaml],
        output="screen",
    )

    # Twist2CMD
    turtle_traj_gen_yaml = os.path.join(
        get_package_share_directory("turtle_trajectory_generator"), "configs", "turtle_traj_gen.yaml"
    )

    twist2cmd = Node(
        package="turtle_trajectory_generator",
        name="turtle_twist2cmd",
        executable="twist2cmd",
        parameters=[turtle_traj_gen_yaml],
        output="screen",
    )

    # Create the launch description and populate
    ld = LaunchDescription()

    # Declare the launch options
    ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_use_rviz_cmd)

    # Load nodes (actions)
    ld.add_action(rqt_graph_node)
    ld.add_action(rviz_cmd)
    ld.add_action(turtle_vis)
    ld.add_action(turtle_ctrl)
    ld.add_action(twist2cmd)

    return ld
