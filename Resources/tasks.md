Intro 15 min

Spike 1.1 Linux 101: terminal, FS, binaries
	25 min

Spike 1.2 ROS background: Type of comm interfaces
	40 min

M1: Background Knowledge

Task 0.1 Problem Description
	35 min

Task 0.2 Installation check up
	15 min


Task 1.1: Create ROS Workspace
	25 mins

Task 2: Get a standard  ros package
	30 min
	
M2 Problem and ROS 
 
Day 1
	
Task 3: Ros2 message package
	60 min
	
Task 4: Ros2 Visualization module
	100 min

M3 Turtle in Rviz

Task 5: Launch Files
	45 min
	
Task 6: Ros2 Control module
	100 min
	
M4 Turtle Control

Day 2

	
Task 7: Ros2 Trajectory Generator package
	100 min

Task 8: System Evaluation
	60 min

Task 9: Recording and playing Bagfiles
	50 min

Day 3 Playing with the Turtle
M5
	
	670 Hrs

	
3.5
7
7

787
