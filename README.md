# ROS2 Crash course

## Table of contents

1. [Problem Description](#problem-description)
1. [Installation](#install-necessary-packages)
2. [Task 1: Workspace](#task-1-create-your-new-workspace-overlay)
3. [Task 2: Get ros package](#task-2-get-teleop-ros-package-and-modify-it)
4. [Task 3: Ros2 message package](#task-3-create-a-new-ros2-cpp-package-turtle-msgs)
5. [Task 4: Ros2 Control package](#task-4-create-a-new-ros2-cpp-package-turtle-ctrl)
6. [Task 5: Launch Files](#task-5-let-us-create-a-lunch-file-to-automate-the-above-process)
7. [Task 6: Control](#task-6-create-the-controller)
8. [Task 7: Ros2 Trajectory Generator package](#task-7-trajectory-generator)
9. [Task 8: System Evaluation](#task-8-evaluate-full-system)
10. [Task 9: Recording and playing Bagfiles](#task-9-record-data-and-play-it-bagfiles)

---

## Problem description

[back](#table-of-contents)

We want to control the motion of a turtle

![Turtle1](/Resources/Figures/Problem/01_t.png "Turtle")

The turtle receives a target pose (x,y,theta) with respect to a world coordinate frame. We need to generate the sequence of poses (control) to move the turtle from the current position to the target position.

![Turtle2](/Resources/Figures/Problem/02_t.png "Turtle control")

We also want to monitor the motion in a 3D visualization system.

![Turtle3](/Resources/Figures/Problem/03_t.png "Turtle vis")

We need to command the target pose of the turtle in two ways. First, using the keyboard. Second, with a trajectory generator that computes a smooth trajectory from the current position to the target position.

![Turtle4](/Resources/Figures/Problem/04_t.png "Turtle commands")

A solution to solve the problem is to divide it into five modules:

First, a module that receives continuously the current turtle position and displays it in the visualization system. The input of this module is a **TurtleState** message with the current turtle's position. The output is the visualization of the turtle.

![BD1](/Resources/Figures/Problem/01_bd.png "Visualization Module")

Then, a control module receives the commanded poses and computes the motions to reach them. The input of this module is the commanded turtle position (continuous or discrete). The output is the current position of the turtle (continuous). Both the input and output are of the same type **TurtleState**.

![BD2](/Resources/Figures/Problem/02_bd.png "Control Module")

We will use two methods to generate the commanded turtle position. One is based on a Spline generator which computes the sequences of turtle poses to reach the target position from the current position. The input is a service where a user can define the target pose and the time to reach it. The output is a continuous sequence of turtle poses of the type **TurtleState**.

![BD3](/Resources/Figures/Problem/03_bd.png "TG Spline Module")

We will also use keyboard commands. To this aim, we will use a standard ros2 package that gets the input of the keyboard and generates a discrete **Twist** velocity message. The keyboard commands will change the velocity of the turtle, both linear and angular.

![BD4](/Resources/Figures/Problem/04_bd.png "Keyboard Module")

Since the control module receives **TurtleState** messages as input, we need to transform the twist velocity message into continuous turtle commands (**TurtleState**). Therefore, we need to implement a module that transforms **Twist** velocities into **TurtleState** poses.

The user can switch between the Keyboard commands and the Spline trajectory.

![BD5](/Resources/Figures/Problem/05_bd.png "Twist2cmd Module")

Our goal is to use the ROS2 middleware to implement these modules and solve the problem.

---

## Install necessary packages

[back](#table-of-contents)

For this course, you should have installed ros foxy in Ubuntu 20.04, and followed the beginner's tutorials (C++ and Python):

Ros2 installation:

<https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html>

Ros2 Tutorials:

<https://docs.ros.org/en/foxy/Tutorials.html>

The following packages provide useful tools and you should also install them.

### Ubuntu and ROS packages

In a new terminal (CTRL+ALT+t)

    sudo apt update
    sudo apt upgrade
    sudo apt install git ssh ros-foxy-teleop-twist-keyboard terminator python3-colcon-common-extensions

#### Set up colcon_cd

On Linux / macOS the above instructions install the package colcon-cd which offers a command to change to the directory a package specified by its name is in. To enable this feature you need to source the shell script provided by that package. The script is named colcon_cd.sh. For convenience you might want to source it in the user configuration, e.g. ~/.bashrc:

    echo "source /usr/share/colcon_cd/function/colcon_cd.sh" >> ~/.bashrc
    echo "export _colcon_cd_root=/opt/ros/foxy/" >> ~/.bashrc

Testing your installation. Open a new terminal (CTRL+ALT+t), and run:

    colcon_cd std_msgs

You will be cd to the path where the ros package std_msgs is located. To verify it, run in the terminal:

    pwd

It should show the following path:

    /opt/ros/foxy/share/std_msgs

### Visual Studio Code

Install VSCode. Please follow the instructions "Installing Visual Studio Code with apt" from (<https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-20-04/>)

Install the following vscode extensions: (<https://code.visualstudio.com/docs/editor/extension-marketplace>)

- C/C++
- catkin-tools
- CMake
- Cmake Tools
- Pylance
- Python
- Python for VSCode
- Python preview
- ROS
- ROS snippets
- ROS2

---

## Task 1 Create your new workspace overlay

[back](#table-of-contents)

 mkdir -p ~/ros2/workspaces/ros2_crash_ws/src

---

## Task 2 Get teleop ros package and modify it

[back](#table-of-contents)

### 2.1 Clone the ros package repo

    cd ~/ros2/workspaces/ros2_crash_ws/src/
    git clone https://github.com/ros2/teleop_twist_keyboard.git

Source the underlay (foxy)

    source /opt/ros/foxy/setup.bash

### 2.2 Compile the workspace

    cd ~/ros2/workspaces/ros2_crash_ws/
    colcon build

### 2.3 Run the binary teleop_twist_keyboard.py and play with it

Open a new terminal:

    source /opt/ros/foxy/setup.bash
    ros2 run teleop_twist_keyboard teleop_twist_keyboard

You can use the keyboard to publish twist commands. Read the instructions in the terminal.

**Q1**: How can we test our program? Hint: ros2 topic

### 2.4 Modify the node teleop_twist_keyboard.py

a) Add a customized print message in the file

    ~/ros2/workspaces/ros2_crash_ws/src/teleop_twist_keyboard/teleop_twist_keyboard.py 

b) Compile your workspace

c) Open a new terminal (CTRL+ALT+t) and run

    source /opt/ros/foxy/setup.bash
    ros2 run teleop_twist_keyboard teleop_twist_keyboard

**Q2**: What happened? Can you see your custom message? Hint: underlay vs overlay

### 2.5 Modify the binary (Python script) teleop_twist_keyboard.py to generate the following motions

  Linear motions:

    u: left-front
    i: front
    o: right-front
    j: left
    k: stop
    l: right
    m: left-back
    ,: back
    .: right-back

    Angular Motions:
    w: counter-clockwise
    e: stop
    r: clock-wise

  Speed:

    + increase linear speed +0.1
    - decrease linear speed -0.1
    * increase angular speed +0.1
    / decrease angular speed -0.1

### 2.6 Compile your workspace

    cd ~/ros2/workspaces/ros2_crash_ws/
    colcon build

### 2.7 Test your new node

Open a new terminal and source your workspace (overlay)

    cd ~/ros2/workspaces/ros2_crash_ws/
    source install/setup.bash
    ros2 run teleop_twist_keyboard teleop_twist_keyboard --ros-args --remap /cmd_vel:=turtle_twist

**Q3**: Does it work? Can you see your changes? How can you verify it? Hint: use ros2 topic

---

## Task 3 Create a new ros2 cpp package turtle msgs

[back](#table-of-contents)

### 3.1 Use the ros2 create package function

    cd ~/ros2/workspaces/ros2_crash_ws/src
    ros2 pkg create --build-type ament_cmake turtle_msgs

A filesystem will be created with the correct structure for a ros2 c++ package:

```bash
.
└── ros2_crash_ws
    └── src
        └── turtle_msgs
            ├── CMakeLists.txt
            ├── include
            │   └── turtle_msgs
            ├── package.xml
            └── src
```

**turtle_msgs**: is a folder containing our ros2 package

**CMakeLists.txt**: CMake configuration file. Here we specify the dependencies of these packages, which files this package will provide, and much more.

**include/turtle_msgs**: This folder contains the c++ headers provided by this package.

**package.xml**: Provides general information of this package. This file is also used by other ros2 packages to define dependencies.

**src**: This folder will contain the source files of the binaries that this ros package provides. The binaries can have the form of executables (Nodes) or libraries that can be used by other ros2 packages.

### 3.2 Compile the new package

In the terminal where you have sourced the underlay (foxy), compile your workspace. If you closed it, open a
new terminal, move to your workspace folder and source the underlay (foxy).

    colcon build  --symlink-install --merge-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1

**Q4**: Any problems? Do you notice any difference with the additional compiling arguments?

### 3.3 Create a custom turtle message (TurtleStateStamped)

a) Create a ~/ros2/workspaces/ros2_crash_ws/src/turtle_msgs/msg folder with a file named
TurtleStateStamped.msg

add the following text:

```bash
    # Defines the state of the turtle 

    # Header with time and parent link
    std_msgs/Header header

    # Turtle pose (x,y,theta) wrt world
    geometry_msgs/Pose2D pose

    # Turtle Velocity wrt world
    geometry_msgs/Twist vel
```

b) Modify the CMakeLists.txt and package.xml to include the new message. Hint: see the reference files and
modify them accordingly

c) Compile the workspace. Hint: overlay vs underlay

### 3.4 Verify that your new message has been created

Hint: use ros2 interface

You should see something similar to this:

```bash
    # Defines the state of the turtle 

    # Header with time and parent link
    std_msgs/Header header

    # Turtle pose (x,y,theta) wrt world
    geometry_msgs/Pose2D pose

    # Turtle Velocity wrt world
    geometry_msgs/Twist vel
```

**Q5**: Can you find your new message? Hint: overlay vs underlay

---

## Task 4 Create a new ros2 cpp package turtle ctrl

[back](#table-of-contents)

This ros package will contain the visualization and control modules (C++).

This new package will use the previously created package turtle_msgs. In other words, turtle_msgs is a dependency
for this new package.

### 4.1 Use the ros2 create package function

    cd ~/ros2/workspaces/ros2_crash_ws/src
    ros2 pkg create --build-type ament_cmake turtle_ctrl

### 4.2 Compile the new package

You can compile the complete workspace:

    colcon build  --symlink-install --merge-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1

or just the new package (and its dependencies)

    colcon build  --packages-select turtle_ctrl --symlink-install --merge-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1

### 4.3 Create the TurtleVis Class

The turtleVis class will provide a library with the functions needed to deploy the visualization module. The input is a continuous topic (**TurtleStateStamped**) with the current turtle's position. The output is a visualization marker and the corresponding **tf** frame.

![TurtleVisModule](/Resources/Figures/Modules/turtleVis.png "Turtle Visualization Module")

Copy the TurtleVis class files TurtleVis.h and TurtleVis.cpp to their corresponding folders

### 4.4 Modify the CMakeFile

Modify the CMakeFile to create a shared library with the TurtleVis class

### 4.5 Copy the turtle visualization node

Copy the application file turtle_vis.cpp

### 4.6 Add the new node into the CMakeFile

Modify the CMakeFile to create a new binary (Exec)

### 4.7 Compile your workspace (or single ros package)

Hint: underlay or overlay?

### 4.8 Copy the configuration and parameters files

a) Copy the file "turtle.rviz" into turtle_ctr/rviz/turtle.rviz

b) Copy the files "turtle_ctrl.yaml" and "turtle_vis.yaml" into the folder turtle_ctr/configs/

### 4.9 Test your application

You will need three terminals. You need to source your workspace (overlay) in each of them.

#### Terminal 1: Turtle Visualization node

    ros2 run turtle_ctrl turtle_vis --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_ctrl/config/turtle_vis.yaml
  
#### Terminal 2: Rviz

    ros2 run rviz2 rviz2 -d ~/ros2/workspaces/ros2_crash_ws/src/turtle_ctrl/rviz/turtle.rviz
  
#### Terminal 3: Publish a TurtleState topic

    ros2 topic pub --rate 1 /turtle_pose turtle_msgs/msg/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

You should see something similar to this:

![TurtleVis](/Resources/Figures/TurtleVis/01_turtle.png "Turtle Visualization")

**Q6**: What can you see? Hint: Change the fixed frame from "map" to "world" in rviz

### 4.10 Change the position and orientation of the turtle with a new message

---

## Task 5 Let us create a lunch file to automate the above process

[back](#table-of-contents)

### 5.1 Copy the launch file

Copy the file "turtle_vis_test_launch.py" into the folder turtle_ctrl/launch/

### 5.2 Run the new launch file

Stop the running nodes (process) in all the terminals (CTRL+c).

#### Terminal 1: Launch file

    ros2 launch turtle_ctrl turtle_vis_test_launch.py

#### Terminal 2: Publish a TurtleState topic

    ros2 topic pub --rate 1 /turtle_pose turtle_msgs/msg/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

You should get the same output as before:

![TurtleVis](/Resources/Figures/TurtleVis/01_turtle.png "Turtle Visualization")

**Q7**: Does it work? What happened?

---

## Task 6 Create the controller

[back](#table-of-contents)

The control module should receive the target pose of the turtle (Continuous or Discrete), and it should generate a continuous topic with the turtle's position. Then, we have two different threads. One is triggered by the target pose topic, and the other computes the new turtle's position and publishes it. This module should also publish a **tf** frame for the target position. 

![TurtleCtrlModule](/Resources/Figures/Modules/turtleCtrl.png "Turtle Control Module")

### 6.1 Copy the TurtleCtrl class

Copy the class files TurtleCtrl.cpp and TurtleCtrl.h to the corresponding folders

### 6.2 Modify the CMakeFile

Modify the CMakeFile to create a shared library with the control class

### 6.3 Copy the turtle control node

Copy the application file turtle_ctrl.cpp

### 6.4 Add the new node into the CMakeFile

Modify the CMakeFile to create a new binary (Exec)

### 6.5 Compile your workspace (or single ros package)

### 6.6 Test your application

a) Copy the launch file turtle_vis_test_launch.py and rename it as turtle_test_launch.py

b) Modify the new launch file to include the control node

### 6.7 Run the new launch file

ros2 topic pub --rate 1 /turtle_cmd turtle_msgs/msg/TurtleStateStamped "{pose: {x: 1, y: 1, theta: 0}}"

**Q8**: How can you test your application? Hint: ros2 topic

---

## Task 7 Trajectory generator

[back](#table-of-contents)

We will implement two methods to generate the commanded turtle pose for the controller.

First, transform the Twist (turtle velocity) message generated from the node teleop_twist_keyboard to continuous commanded turtle poses.

Second, we will create a node that provides a service to request a desired turtle pose. This node will use the requested turtle pose and generate a continuous smooth trajectory for the commanded turtle pose.

The output of both nodes is a TurtleStateStamped message (/turtle_cmd), which is connected to the controller to change the pose of the turtle.

### 7.1 Create a python ros package

Create a new ros python package "turtle_trajectory_generator"

    ros2 pkg create --build-type ament_python turtle_trajectory_generator

A filesystem will be created with the correct structure for a ros2 python package:

```bash
.
└── ros2_crash_ws
    └── src
        └── turtle_trajectory_generator
            ├── package.xml
            ├── resource
            │   └── turtle_trajectory_generator
            ├── setup.cfg
            ├── setup.py
            ├── test
            │   ├── test_copyright.py
            │   ├── test_flake8.py
            │   └── test_pep257.py
            └── turtle_trajectory_generator
                └── __init__.py
```

**src/turtle_trajectory_generator**: is a folder that contains our ros2 python package

**package.xml**: Provides general information of this package. This file is also used by other ros2 packages to define dependencies.

**setup.py**: this is a file to configure the installation and paths for python. You will be constantly modifying this file by adding script names and paths. This information is also used by other ros packages, for example, to use python modules defined in this package.

**turtle_trajectory_generator/turtle_trajectory_generator**: This folder contains all the binary files of this package. The binary files can be python scripts or python modules. You can create a folder named **scripts** to contain the python scripts. The python modules can be located directly in the folder.

The rest of the folders are needed to configure the python package, and, usually, you don't need to change them.

### 7.2 Set up your new ros package

We need to modify the package.xml file. This ros package is a ros2 python package. Then, we need to provide information on where to install the python scripts and modules used by ros2 run, ros2 launch, and other ros packages. This information is provided in the files package.xml, resource, setup.py, etc.

### 7.3 Add python scripts

Copy the modified file teleop_twist_keyboard.py from the ros package teleop_twist_keyboard to this new package.

### 7.4 Configure your python ros2 package

Modify the setup.py file to install the python scripts and modules

### 7.5 Compile the workspace

Even when this is a ros2 python package, this step is needed to install the python scripts and modules and create symbolic links. In this way, other ros packages can use the scripts and modules provided by this ros2 package.

    colcon build --symlink-install --merge-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1

**--symlink-install**: this compilation flag is needed to create symbolic links in the installation path. In this way, when we modify the scripts or modules, the changes will be reflected in the installed files. Therefore, no recompilation is needed when modifying python files.

**NOTE**: If you are using VSCode with the correct .vscode/tasks.json file, you can compile the workspace with CTRL+Shift+b, and select the option:

    colcon_build

### 7.6 Test your python script

a) First, remove the folder **~/ros2/workspaces/ros2_crash_ws/src/teleop_twist_keyboard**. We don't need it since we have ported the python script teleop_twist_keyboard.py to our new ros2 python package.

b) Run the script using the ros2 package turtle_trajectory_generator.

**Q9**: How can you verify the new python script? Hint: ros2 topic

### 7.7 Create the desired pose service

We need to create a new service to set the desired turtle pose. We are using the ros package turtle_msgs as the main package to contain all the messages and services of this project.

a) Create the folder turtle_msgs/srv

b) Copy file SetDesiredPose.srv into the new folder turtle_msgs/srv

c) Modify the CMakeFile to include the new service. You need to add the lines:

    set(srv_files
    "srv/SetDesiredPose.srv"
    )

    rosidl_generate_interfaces(${PROJECT_NAME}
      ${msg_files}
      ${srv_files}
      DEPENDENCIES builtin_interfaces std_msgs geometry_msgs
    )

d) Compile the ros2 package turtle_msgs

**Q10**: How can you verify that the new service is properly defined? Hint: ros2 interface show  

### 7.8 Unresolved python symbols

You may get an error message informing you that some modules cannot be resolved. You need to include the paths of the ros packages in python configuration file.

a) Configure .vscode/setting.json python.autoComplete.extraPaths and python.analysis.extraPaths by adding:

    "python.autoComplete.extraPaths": [
        "${workspaceFolder}/src/turtle_trajectory_generator",
        "${workspaceFolder}/install/lib/python3.8/site-packages",
        "/opt/ros/foxy/lib/python3.8/site-packages",
        "/usr/lib/python3/dist-packages/"
    ],
    "python.analysis.extraPaths": [
        "${workspaceFolder}/src/turtle_trajectory_generator",
        "${workspaceFolder}/install/lib/python3.8/site-packages",
        "/opt/ros/foxy/lib/python3.8/site-packages",
        "/usr/lib/python3/dist-packages/"
    ],

### 7.9 Add Python modules

a) Create the trajectory generator python module. Use the file trajectory_generator_module.py and copy it to the correspondent python modules folder turtle_trajectory_generator/turtle_trajectory_generator

b) Configure your python package and include the new modules. You need to modify the setup.py file.

c) Compile the workspace again.

### 7.10 Add python script Twist2CMD

We need to develop a module that receives a Twist velocity message and transforms it into commanded TurtleStateStamped message.

![Twist2CMDModule](/Resources/Figures/Modules/twist2cmd.png "Turtle Twist2CMD Module")

We need to include some python scripts that will use the classes defined in the python module (library).

a) Copy the file twist2cmd.py to the scripts folder turtle_trajectory_generator/turtle_trajectory_generator/scripts

This script converts a Twist message to commanded turtle pose message (TurtleStateStamped) needed as a reference in the controller.

b) Add the script in the setup.py file

c) Compile your ros package

### 7.11 Test your new script

a) Create a folder turtle_trajectory_generator/configs

b) Copy the parameters file turtle_traj_gen.yaml file into the new folder

#### Terminal 1: Keyboard node

    ros2 run turtle_trajectory_generator teleop_twist_keyboard  --ros-args --remap /cmd_vel:=turtle_twist

#### Terminal 2: Twist2cmd

    ros2 run turtle_trajectory_generator twist2cmd --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml

**Q11**: How can you verify your script? Hint: ros2 topic, rqt_plot

If you use rqt_plot, you should see something similar to this:

![TurtleVis](/Resources/Figures/Keyboard/02_plot.png "Plot with the turtle motions")

### 7.12 Add python script Polynomial Trajectory

We need a node that provides a service where the user can request a desired turtle pose. The python script with the node will use this desired pose and generate a smooth trajectory from the current turtle pose to the desired one.

![PolyTrajectory](/Resources/Figures/Modules/polyTrajectory.png "Turtle Polynomial Trajectory Module")

a) Copy the file poly_trajectories.py into the scripts folders

b) Add the script in the setup.py file

c) Compile your ros package

### 7.13 Test your new script

#### Terminal 1: Polynomial trajectory generation

    ros2 run turtle_trajectory_generator poly_trajectory --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml

**Q12**: Can you find the description of the new service? Hint: ros2 service

#### Terminal 2: Call the service

    ros2 service call /turtle_poly/set_desired_pose turtle_msgs/srv/SetDesiredPose "{ turtle_d:{x: 1, y: 1, theta: 2}, time: 5}"

**Q13**: How can you test the trajectory generator nodes only? Hint: ros2 run rqt_plot rqt_plot
**Q14**: Does it work? Hint: did you configure your setup.py file?

Again, using rqt_plot, you should see something like this:

![TurtleVis](/Resources/Figures/Poly/02_plot.png "Plot with the turtle motions")

---

## Task 8 Evaluate full system

[back](#table-of-contents)

We need to test the two versions of the system. One where we can control the turtle with the keyboard and the other where we can control the turtle by defining the desired turtle's position.

### 8.1 Turtle Control and visualization

For both tests, we need the turtle control node and the visualization (rviz).

#### Terminal 1: Launch file with visualization and control

    ros2 launch turtle_ctrl turtle_test_launch.py

### 8.1 Keyboard control demo

#### Terminal 2: keyboard

    ros2 run turtle_trajectory_generator teleop_twist_keyboard  --ros-args --remap /cmd_vel:=turtle_twist

#### Terminal 3: twist2cmd and ros parameters

    ros2 run turtle_trajectory_generator twist2cmd --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml

**Q14**: Can you see the turtle moving with the keyboard commands? Hint: Verify all the topics and services. Are all of them correctly configured?

You should see the turtle moving whit the keyboard commands:

![TurtleVis](/Resources/Figures/Keyboard/01_turtle.png "Turtle Visualization")

![TurtleVis](/Resources/Figures/Keyboard/02_plot.png "Plot with the turtle motions")

### 8.2 Polynomial Trajectory

#### Terminal 2: polynomial trajectory generator node

    ros2 run turtle_trajectory_generator poly_trajectory --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml

#### Terminal 3: Calling the service

    ros2 service call /turtle_poly/set_desired_pose turtle_msgs/srv/SetDesiredPose "{ turtle_d:{x: 1, y: 1, theta: 2}, time: 5}"

**Q15**: Can you see the turtle moving with the keyboard commands? Hint: Verify all the topics and services. Are all of them correctly configured?

You should see the turtle moving smoothly from the initial position to the target position in the given time:

![TurtleVis](/Resources/Figures/Poly/01_turtle.png "Turtle Visualization")

![TurtleVis](/Resources/Figures/Poly/02_plot.png "Plot with the turtle motions")

---

## Task 9 Record data and play it Bagfiles

[back](#table-of-contents)

In this task, we will move the turtle using the keyboard commands and record the commanded turtle's trajectory. Then,
we will replay the trajectory.

### 9.1 Recording data

First, we will record the produced data into bag files. For this, you will need five terminals.

#### Terminal 1: Launch files

    ros2 launch turtle_ctrl turtle_test_launch.py

#### Terminal 2: twist2cmd and ros parameters

    ros2 run turtle_trajectory_generator twist2cmd --ros-args --params-file ~/ros2/workspaces/ros2_crash_ws/src/turtle_trajectory_generator/configs/turtle_traj_gen.yaml

#### Terminal 3: keyboard

    ros2 run turtle_trajectory_generator teleop_twist_keyboard  --ros-args --remap /cmd_vel:=turtle_twist

#### Terminal 4: RQT_PLOT  

    ros2 run rqt_plot rqt_plot

#### Terminal 5: Bagfile recording

First, we need to identify which data (topic) we would like to record. This can be done by analyzing the node
communication graph.

    ros2 run rqt_graph rqt_graph

We identify the topic **/turtle_cmd** as the target one. Then, we can record that topic:

    cd ~/ros2/workspaces/ros2_crash_ws/
    mkdir bags/
    cd bags/
    ros2 bag record -o turtle_cmd_bag /turtle_cmd

Now, move the turtle with the keyboard in Terminal 3. Record a few seconds and stop the nodes in the terminals T2, T3,
and T5 (twist2cmd, keyboard, and bag, respectively).

You will find a new folder named **turtle_cmd_bag** that contains the recorded data.

You can get some information about the bag file using the command:

    ros2 bag info turtle_cmd_bag

### 9.2 Playback the recorded data

Now, we will play the topic /turtle_cmd and see the motion in Rviz.

#### Terminal 5: Replay bagfile

    ros2 bag play turtle_cmd_bag

The last command will replay the topic /turtle_cmd one time. If we need to replay in a loop, we can use a special flag:

    ros2 bag play turtle_cmd_bag -l

**Q16**: Can you see the turtle moving?
